package com.cv.sbi.exception;

public class MISException extends Exception {
    public MISException(String message) {
        super(message);
    }

    public MISException(Throwable cause) {
        super(cause);
    }
}
