package com.cv.sbi;

import com.cv.sbi.exception.MISException;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.MongoException;
import com.mongodb.client.AggregateIterable;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import org.apache.commons.lang3.StringUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.vertx.java.core.json.JsonObject;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

public class MISGenerator {

    private static Logger logger = LoggerFactory.getLogger(MISGenerator.class);

    public static JsonObject readPropsFile(String path) throws IOException {
        System.out.println("Reading the properties from File");

        String inputJson = readJsonFromFile(path);
        JsonObject propsJsonObj = new JsonObject(inputJson);

        return propsJsonObj;
    }
    private static List<String> HEADERS;
    public static String readJsonFromFile(String path) throws IOException {
        StringBuilder buffer = new StringBuilder();
        String inputJson = null;
        try (BufferedReader br = new BufferedReader(new InputStreamReader(new FileInputStream(path), "UTF-8"))) {
            String line;
            while ((line = br.readLine()) != null) {
                buffer.append(line);
            }
            br.close();

            inputJson = buffer.toString();
        }

        return inputJson;
    }

    public static void main(String[] args) throws Exception {
        String propertiesFilePath = System.getProperty("propertyFile");
        String startDateStr = System.getProperty("startDate");
        String endDateStr = System.getProperty("endDate");
        String noOfDaysStr = System.getProperty("maxDays");
        String cronjob= System.getProperty("cronjob");
        int noOfDays = 10;
        if (org.apache.commons.lang3.StringUtils.isNoneBlank(noOfDaysStr)) {
            try {
                noOfDays = Integer.parseInt(noOfDaysStr);
            } catch (NumberFormatException ne) {
                ne.printStackTrace();
            }

        }
        if (StringUtils.isBlank(propertiesFilePath)) {
            throw new MISException("Properties file is not present in the arguments");
        }

        //Reading properties file
        JsonObject propObject = readPropsFile(propertiesFilePath);
        JsonObject mongoConf = propObject.getObject("mongo");
        String dbName = mongoConf.getString("dbName");
        String userName = mongoConf.getString("username");
        String password = mongoConf.getString("password");
        String host = mongoConf.getString("host");
        int port = mongoConf.getInteger("port", 27017);
        boolean ssl = mongoConf.getBoolean("ssl", false);

        String tenantId = propObject.getString("tenantId", "1");
        String outputUrl = propObject.getString("outputURL");
        String apiKey = propObject.getString("apiKey");
        String consumerId = propObject.getString("consumerId");
        String misFolderPath = propObject.getString("misFolderPath");

        String commaSeparatedHeaders = propObject.getString("headers");
        if(StringUtils.isBlank(commaSeparatedHeaders)){
            throw new MISException("Headers were not given in properties fils");
        }
        HEADERS = Arrays.asList(commaSeparatedHeaders.split(","));
        validateRequiredFieldsFromPropsFile(propertiesFilePath, mongoConf, dbName, userName, password, host, outputUrl,
                apiKey, consumerId, misFolderPath);


        SimpleDateFormat format = new SimpleDateFormat("yyyy-MM-dd");
        Date startDate;
        Date endDate;

        if("true".equalsIgnoreCase(cronjob)) {
            Calendar calendar = Calendar.getInstance();
            endDate = calendar.getTime();
            calendar.add(Calendar.DATE, -1);
            startDate = calendar.getTime();
        } else {
            try {
                startDate = format.parse(startDateStr);
                endDate = format.parse(endDateStr);
            } catch (ParseException e) {
                throw new MISException("One of the given date is not in proper format, \"yyyy-MM-dd\" is the required format");
            }
            long difference = (startDate.getTime() - endDate.getTime()) / 86400000;
            if (Math.abs(difference) > noOfDays) {
                throw new MISException("Dates should have a max difference of " + noOfDays + " days");
            }
        }


        MongoClient mongo = null;
        MongoDatabase mongoDb = null;
        try {
            MongoClientURI URL = new MongoClientURI(createMongoURL(dbName, userName, password, host, port, ssl));
            mongo = new MongoClient(URL);
            mongoDb = mongo.getDatabase(dbName);
            //endDate = incrementDateByOneDay(endDate);
            while (endDate.after(startDate) && endDate.compareTo(startDate)!=0) {
                String queryStartDate = format.format(startDate);
                //Increment the date
                startDate = incrementDateByOneDay(startDate);
                String queryEndDate = format.format(startDate);
                System.out.println("Started for date " + queryStartDate);
                String fileName = "MIS_" + queryStartDate + ".csv";
                try (BufferedWriter writer = new BufferedWriter(new FileWriter(misFolderPath + File.separator
                        + fileName))) {
                    writeHeaders(writer);
                    AggregateIterable<Document> aggregateResult = getAggregateResultFromMongo(queryStartDate, queryEndDate,
                            mongoDb, tenantId);
                    int count = 0;
                    for (Document current : aggregateResult) {
                        count++;
                        System.out.println("Inside aggregate result");
                        JsonObject tempObjectForWritingCSV = new JsonObject();
                        String userId = current.getString("userId");
                        System.out.println(userId);
                        Object data = current.get("data");
                        if (data != null && data instanceof Document) {
                            Document resultDoc = (Document) data;
                            if (resultDoc != null) {
                                tempObjectForWritingCSV.mergeIn(getJsonObject((Document) resultDoc.get("creditDecision")));
                                tempObjectForWritingCSV.mergeIn(getJsonObject((Document) resultDoc.get("creditScore")));
                                tempObjectForWritingCSV.putString("uniqueId", resultDoc.getString("uniqueId"));
                                tempObjectForWritingCSV.putString("scoreVersion", resultDoc.getString("scoreVersion"));
                            }
                        }
                        tempObjectForWritingCSV.putString("userId", userId);

                        Document sdk_user_result = getDocumentForUser(mongoDb, userId, tenantId, "sdk_users");
                        tempObjectForWritingCSV.mergeIn(getJsonObject(sdk_user_result));

                        Document userL4Document = getDocumentForUser(mongoDb, userId, tenantId, "user_l4");
                        if (userL4Document != null && userL4Document.get("data") != null) {
                            Document l4Doc = (Document) userL4Document.get("data");
                            tempObjectForWritingCSV.mergeIn(getJsonObject((Document) l4Doc.get("cibil"), "cibil"));
                            tempObjectForWritingCSV.mergeIn(getJsonObject((Document) l4Doc.get("application")));
                            tempObjectForWritingCSV.mergeIn(getJsonObject((Document) l4Doc.get("alternatePolicy")));
                        }
                        String uniqueId = tempObjectForWritingCSV.getString("uniqueId", null);
                        if (StringUtils.isBlank(uniqueId)) {
                            uniqueId = tempObjectForWritingCSV.getString("loginName");
                        }
                        if (StringUtils.isNotBlank(uniqueId)) {
                            JsonObject outputResponseObj = getVariablesFromAPI(uniqueId, outputUrl, consumerId, apiKey);
                            if (outputResponseObj != null && outputResponseObj.getObject("data") != null) {
                                tempObjectForWritingCSV.mergeIn(outputResponseObj.getObject("data"));
                            }
                        }
                        System.out.println("Writing data for " + count);
                        writeDataToFile(tempObjectForWritingCSV, writer);
                    }
                }
            }
        } catch (MongoException ex) {
            ex.printStackTrace();
        } finally {
            if (mongo != null) {
                mongo.close();
            }
        }
    }

    private static void writeHeaders(BufferedWriter writer) throws IOException {
        for (String header : HEADERS) {
            writer.append(header).append(",");
        }
        writer.append("\n");
    }

    private static Date incrementDateByOneDay(Date startDate) {
        Calendar calendar = Calendar.getInstance();
        calendar.setTime(startDate);
        calendar.add(Calendar.DATE, 1);
        return calendar.getTime();
    }

    private static String createMongoURL(String dbName, String userName, String password, String host, int port, boolean ssl) {
        String mongoUrL = "mongodb://" + userName + ":" + password
                + "@" + host + ":" + port + "/" + dbName + "?ssl=" + (ssl ? "true" : "false");
        System.out.println(mongoUrL);
        return mongoUrL;
    }

    private static void validateRequiredFieldsFromPropsFile(String propertiesFilePath, JsonObject mongoConf, String dbName, String userName, String password, String host, String outputUrl, String apiKey, String consumerId, String misFolderPath) throws MISException {
        if (mongoConf == null || StringUtils.isBlank(dbName) || StringUtils.isBlank(userName)
                || StringUtils.isBlank(password) || StringUtils.isBlank(host)) {
            throw new MISException("Mongo configuration is not complete, Please check \"" + propertiesFilePath + "\" file");
        }

        if (StringUtils.isBlank(outputUrl)) {
            throw new MISException("Output URL is not provided in the configuration, Please check \"" + propertiesFilePath + "\" file");
        }

        if (StringUtils.isBlank(apiKey) && StringUtils.isBlank(consumerId)) {
            throw new MISException("Both CONSUMERID and APIKEY are missing in configuration, Please check \"" + propertiesFilePath + "\" file");
        }

        if (StringUtils.isBlank(misFolderPath)) {
            throw new MISException("Output Folder to save MIS is not provided in the configuration, Please check \"" + propertiesFilePath + "\" file");
        }
    }

    private static JsonObject getVariablesFromAPI(String uniqueId, String outputUrl, String consumerId, String apiKey) {
        System.out.println("Get output for uniqueId " + uniqueId);
        JsonObject responseObjct = null;

        HttpURLConnection connection = null;

        try {
            URL url = new URL(outputUrl);
            connection = (HttpURLConnection) url.openConnection();
            connection.setRequestMethod("POST");
            connection.setRequestProperty("apikey", apiKey);
            connection.setRequestProperty("X-CONSUMER-ID", consumerId);

            connection.setUseCaches(false);
            connection.setDoOutput(true);

            //Send request
            try (DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream())) {
                JsonObject jsonParam = new JsonObject();
                jsonParam.putString("uniqueId", uniqueId);
                outputStream.writeBytes(jsonParam.toString());
                outputStream.flush();
            }

            //Get response
            StringBuilder response = new StringBuilder();
            InputStream inputStream;
            System.out.println("Status code " + connection.getResponseCode());
            if (connection.getResponseCode() == HttpURLConnection.HTTP_OK) {
                inputStream = connection.getInputStream();
                if (inputStream != null) {
                    try (BufferedReader inputBuffer = new BufferedReader(new InputStreamReader(inputStream))) {
                        String line;
                        while (null != (line = inputBuffer.readLine())) {
                            response.append(line);
                            response.append("\r");
                        }
                    }
                } else {
                    StringBuilder errorBuilder = new StringBuilder();

                    inputStream = connection.getErrorStream();
                    try (BufferedReader inputBuffer = new BufferedReader(new InputStreamReader(inputStream))) {
                        String line;
                        while (null != (line = inputBuffer.readLine())) {
                            errorBuilder.append(line);
                            errorBuilder.append("\r");
                        }
                        System.out.println(errorBuilder);
                    }
                }
                responseObjct = new JsonObject(response.toString());

            }
        } catch (MalformedURLException e) {
            logger.error("Malformed URL exception occurred while calling the Recalc API", e);
        } catch (IOException e) {
            logger.error("Creation of connection failed while calling the Recalc API", e);
        } catch (Exception e) {
            logger.error("Pushing IDMe data failed : ", uniqueId, e);
        } finally {
            if (null != connection) {
                connection.disconnect();
            }
        }
        return responseObjct;
    }

    private static Document getDocumentForUser(MongoDatabase mongoDb, String userId, String tenantId, String
            tableName) {
        return mongoDb.getCollection(tableName).find(Filters.and(Filters.eq("userId", userId),
                Filters.eq("tenantId", tenantId))).sort(new Document("_id", -1)).first();
    }

    private static void writeDataToFile(JsonObject tempObjectForWritingCSV, BufferedWriter writer) throws
            IOException {
        if (tempObjectForWritingCSV.size() != 0) {
            for (String str : HEADERS) {
                String valueToAppend = "Null";
                if (tempObjectForWritingCSV.getValue(str) != null) {
                    valueToAppend = tempObjectForWritingCSV.getValue(str).toString();
                }
                writer.append(valueToAppend).append(",");
            }
            writer.append("\n");
        }
    }

    private static JsonObject getJsonObject(Document document) {
        JsonObject jsonObject = new JsonObject();
        if (document != null) {
            Map<String, Object> map = new HashMap<>(document);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                jsonObject.putValue(entry.getKey(), entry.getValue());
            }
        }
        return jsonObject;
    }

    private static JsonObject getJsonObject(Document document, String key) {
        JsonObject jsonObject = new JsonObject();
        if (document != null) {
            Map<String, Object> map = new HashMap<>(document);
            for (Map.Entry<String, Object> entry : map.entrySet()) {
                jsonObject.putValue(key + "." + entry.getKey(), entry.getValue());
            }
        }
        return jsonObject;
    }

    private static AggregateIterable<Document> getAggregateResultFromMongo(String startDateStr, String
            endDateStr, MongoDatabase db, String tenantId) {
        MongoCollection<Document> tenant_output = db.getCollection("tenant_output");
        List<Bson> pipeLine = new ArrayList<>();
        Object matcher = new Document("createDate", new Document("$gte", startDateStr).append("$lt", endDateStr))
                .append("type", "cre").append("tenantId", tenantId);

        Object group = new Document("_id", new Document("Date", new Document("$substr", Arrays.asList("$createDate", 0, 10)))
                .append("userId", "$userId"))
                .append("createDate", new Document("$last", "$createDate"))
                .append("data", new Document("$last", "$output.data"));
        Object project = new Document("createDate", "$createDate").append("data", "$data")
                .append("userId", "$_id.userId").append("_id", 0);
        pipeLine.add(new Document("$match", matcher));
        pipeLine.add(new Document("$group", group));
        pipeLine.add(new Document("$project", project));

        return tenant_output.aggregate(pipeLine);
    }

}
